# README #

Witaj na repozytorium warsztatów Unity Advanced. Warsztaty odbywają się we wtorki na Zachodniopomorskim Uniwersytecie Technologicznym w Szczecinie, pod patronatem Grupy.Net! Więcej informacji (+ ogłoszenia spotkań):

* Oficjalna strona Grupy.Net: [http://dotnet.zut.edu.pl/](http://dotnet.zut.edu.pl/)
* Oficjalna grupa FB warsztatów: [https://web.facebook.com/groups/825214727576498/](https://web.facebook.com/groups/825214727576498/)
* Grupa FB Grupy.Net: [https://web.facebook.com/groups/grupa.net.wizut/](https://web.facebook.com/groups/grupa.net.wizut/)
* Serwer Discord: [https://discord.gg/TbPFyXk](https://discord.gg/TbPFyXk)

### Po co to repozytorium? ###

Celem repozytorium jest przechowywanie przykładów zagadnień omawianych na spotkaniach.
Branche są posortowane tematycznie wg spotkania którego dotyczyły. Każdy branch zawiera scenę Unity, na której wykorzystywane są techniki i/lub elementy omawiane na zajęciach.

### How do I get set up? ###

Aby ściągnąć branch:

1. Pobierz aplikację SourceTree na swój komputer
2. Załóż konto Atlassian
3. Wejdź do zakładki Branches na tym repozytorium: ( [https://bitbucket.org/Astralius/zut-unity-advanced/branches/](https://bitbucket.org/Astralius/zut-unity-advanced/branches/)
4. Kliknij "Checkout in SourceTree"
5. (tylko 1. raz): W aplikacji SourceTree wskaż lokację na swoim komputerze, gdzie ma zostać skopiowany projekt. Zatwierdź wybór.
6. |Opcja 1|: Wejdź do powyższej lokalizacji i w katalogu Assets/ otwórz plik Sceny (.scene) 
7. |Opcja 2|: Otwórz Unity. W przeglądarce projektów kliknij "Open" i wskaż powyższą lokalizację (folder projektu na dysku).

### Who do I talk to? ###

* W razie pytań czy problemów skontaktuj się z autorem repozytorium: Adrianem Standowicz (preferowany kontakt przez FB lub Discord)